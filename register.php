<?php
  require_once('templates/header.php');
?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="wrapper">
              <h1 id="userLogin">User Login</h1>
              <form class="form-signin">
                      <div class="form-group">
                          <div class="input-group">
                              <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                              <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="input-group">
                              <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                              <input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon1">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="input-group">
                              <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                              <input type="text" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="input-group">
                              <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-ok"></i></span>
                              <input type="text" class="form-control" placeholder="Retype Password" aria-describedby="basic-addon1">
                          </div>
                      </div>

                      <button class="btn btn-success btn-block" type="submit">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
  require_once('templates/footer.php');
?>
