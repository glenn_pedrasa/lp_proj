<?php
   require_once('classes/user.class.php');



  function isUserLoggedIn(){
    return (isset($_SESSION['userid']) and !empty($_SESSION['userid']));
  }

  function getUserFullName(){
    $name = $_SESSION['user_first_name'].' '.$_SESSION['user_last_name'];

    return ucwords($name);
  }
  function userLogin($username, $password){
      $user = new User();

      $userInfo = $user->logIn($username, $password);

      if($userInfo != 0) :

        $_SESSION['userid']           = $userInfo['user_id'];
        $_SESSION['username']         = $userInfo['username'];
        $_SESSION['user_first_name']  = $userInfo['first_name'];
        $_SESSION['user_middle_name'] = $userInfo['middle_name'];
        $_SESSION['user_last_name']   = $userInfo['last_name'];
        $_SESSION['user_address']     = $userInfo['address'];

        return true;

     else:

       return false;

    endif;
  }
  function logout(){
    session_destroy();
    header('Location:index.php');
  }
?>
