<?php

class Connection{
  	private $db_host = 'sql103.netfast.org';
  	private $db_name = 'nf_20666348_users';
  	private $user    = 'nf_20666348';
  	private $pass    = 'kinginam';
    public $db;

  	public function __construct(){
  		try{
          $conn = new PDO("mysql:host=$this->db_host;dbname=$this->db_name", $this->user, $this->pass);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $this->db = $conn;

  	    } catch(PDOException $e){
  	        echo 'ERROR: ' . $e->getMessage();
  	    }
  		return $this->db;

  	}

  	public function getRows($query, $params = []){
  			$stmt =  $this->db->prepare($query);
  	    $stmt->execute($params);
  	    $result = $stmt->fetchAll();

  	    return $result;
  	}

  	public function getRow($query, $params = []){
  			$stmt =  $this->db->prepare($query);
  			$stmt->execute($params);

  			if($stmt->rowCount() > 0){
  					$result = $stmt->fetch();
  					return $result;
  			}
  			return 0;
  	}

  	public function insertRow($query, $params = []){
  		$stmt = $this->db->prepare($query);
  		$stmt->execute($params);

  		return  $this->db->lastInsertId();
  	}

  	public function deleteRow($query, $params = []){
  		$sth = $this->db->prepare($query);
  		return $sth->execute($params);
  	}

  	public function updateRow($query, $params = []){
  		$sth = $this->db->prepare($query);
  		return $sth->execute($params);
  	}

}
