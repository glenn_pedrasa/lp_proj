<?php
  include_once('connection.class.php');

  class User{
      private $conn;

      public function __construct(){
          $this->conn = new Connection;
      }

      public function logIn($username, $password){
          $password = md5($password);
          $query = "SELECT tu.user_id as 'user_id', tu.username as 'username', tu.userpassword as 'userpassword', tud.first_name as 'first_name',
                           tud.middle_name as 'middle_name', tud.last_name as 'last_name', tud.address as 'address'
                    FROM tb_user tu
                    LEFT JOIN tb_user_details tud
                    ON tu.user_id = tud.user_id
                    WHERE tu.username = ? and tu.userpassword = ?
                    LIMIT 1";
          $params = array($username, $password);

          $userInfo = $this->conn->getRow($query, $params);

          return $userInfo;
      }


  }
?>
