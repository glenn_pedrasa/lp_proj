<?php
  require_once('templates/header.php');
?>

<?php
  if(isUserLoggedIn()) :
    echo 'test';
?>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <div class="container">
              <div class="row">
                <a class="navbar-brand" href="#">
                  <img alt="John Doe" src="...">
                </a>
              </div>
            </div>
        </div>
    </div>
</nav>

<div class="container">
  <div class="row">
    <div id="profile" class="col-md-3">
        <div class="panel panel-default">
              <div id="profile-bg"></div>
              <div id="profile-details">
                <h3><?php echo getUserFullName(); ?></h3>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>
                <ul class="clearfix">
                  <li>
                    <h4>Friends</h4>
                    <span>12M</span>
                  </li>
                  <li>
                    <h4>Enemies</h4>
                    <span>1</span>
                  </li>
                </ul>
              </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><h5>About</h5></div>
            <div class="panel-body">
                <ul id="about-details">
                  <li>
                    <label><i class="glyphicon glyphicon-calendar"></i></label>
                    <span>Went to a Recent Event</span>
                  </li>
                  <li>
                    <label><i class="glyphicon glyphicon-user"></i></label>
                    <span>Became Friends with Tomo</span>
                  </li>
                  <li>
                    <label><i class="glyphicon glyphicon-film"></i></label>
                    <span>Worked as Film Actor</span>
                  </li>
                  <li>
                    <label><i class="glyphicon glyphicon-home"></i></label>
                    <span><?php echo  $_SESSION['user_address']; ?></span>
                  </li>
                  <li>
                    <label><i class="glyphicon glyphicon-map-marker"></i></label>
                    <span>From Zambales</span>
                  </li>
                </ul>
            </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading"><h5>Photos</h5></div>
          <div class="panel-body">
              <ul>
                <li>
                  <label><i class="glyphicon glyphicon-user"></i></label>
                  <span>Sample</span>
                </li>
                <li>
                  <label><i class="glyphicon glyphicon-user"></i></label>
                  <span>Sample</span>
                </li>
                <li>
                  <label><i class="glyphicon glyphicon-user"></i></label>
                  <span>Sample</span>
                </li>
                <li>
                  <label><i class="glyphicon glyphicon-user"></i></label>
                  <span>Sample</span>
                </li>
              </ul>
          </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="panel panel-default">
           <div class="panel-body">
               <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div><!-- /input-group -->
           </div>
        </div>
    </div><!--end col-md-5-->

    <div class="col-md-3">
        <div class="panel panel-default">

        </div>
    </div>
  </div>
</div>

<?php
  else:
?>

  <?php
    $username = isset($_POST['username'])?$_POST['username']:'';
    $password = isset($_POST['userpassword']) ? $_POST['userpassword'] :'';

    if($username == '' or $password  == ''){
      ?>
        <div id="loginErrorMessage">Please Fill-in all the Fields</div>
      <?php
    }else{
      if(userLogin($username, $password)){
         header('Location:index.php');
      }else{
      ?>
        <div id="loginErrorMessage">Wrong Credentials</div>
      <?php
      }
    }
  ?>


  <div class="container">
      <div class="row">
          <div class="col-md-4 col-md-offset-4">
              <div class="wrapper">
                <h1 id="userLogin">User Login</h1>
                <form class="form-signin" method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon bg-primary" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" class="form-control" name="userpassword" id="userpassword" placeholder="Password" aria-describedby="basic-addon1">
                            </div>
                        </div>

                        <div class="checkbox clearfix">
                             <label class="pull-left">
                                <input type="checkbox"> Check me out
                             </label>
                             <label class="pull-right">
                                <a href="register.php">Register an Account</a>
                             </label>
                        </div>

                        <button class="btn btn-success btn-block" type="submit">Login</button>
                  </form>
              </div>
          </div>
      </div>
  </div>

<?php
  endif;
?>

<?php
  require_once('templates/footer.php');
?>
